from distutils.core import setup

setup(name='SheepEasyCrypt',
      version='1.0.0',
      description='I always used this collection of packages to encrypt stuff.'
                  ' And I was annoyed of coping it all the time.',
      author='__jcb__',
      url='https://gitlab.com/__jcb__/sheep_easy_crypt/-/tree/main',
      packages=['rsa', 'pycryptodomex'],
     )