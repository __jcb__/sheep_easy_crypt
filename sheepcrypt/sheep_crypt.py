from typing import Union

from rsa import PublicKey, PrivateKey, newkeys, decrypt as rsa_dec, encrypt as rsa_enc
from rsa.pkcs1 import DecryptionError
from Cryptodome.Cipher import AES
from pickle import dumps as pdumps, loads as ploads
from secrets import token_hex



class SheepException(Exception):
    def __init__(self, *args):
        super().__init__(*args)


class SheepEasyCrypt:


    @classmethod
    def get_asym_key(cls, key_size=4096) -> tuple[PublicKey, PrivateKey]:
        return newkeys(key_size)

    @classmethod
    def encry_asym(cls, obj, pub_key: PublicKey) -> bytes:
        return rsa_enc(cls.serial(obj), pub_key)

    @classmethod
    def decry_asym(cls, chiffer: bytes, pri_key: PrivateKey) -> object:
        try:
            return cls.deserial(rsa_dec(chiffer, pri_key))
        except DecryptionError:
            raise SheepException()



    @classmethod
    def encry_sym(cls, obj, key: str) -> dict:  # future_work: need to get a sym encryption package (if possible aes)
        data = pdumps(obj)
        key = cls.__clean_key(key)
        cipher = AES.new(key, AES.MODE_EAX)

        nonce = cipher.nonce
        ciphertext, tag = cipher.encrypt_and_digest(data)
        return {"ciphertext": ciphertext,
                "tag": tag,
                "nonce": nonce}

    @classmethod
    def __clean_key(cls, key):
        return pdumps(key + "kljsdnvvlkjasdvlkjasflvkadsfslvdlfv")[:32]  # keysize 256

    @classmethod
    def decry_sym(cls, chipher_collection: dict, key: str) -> tuple:
        try:
            key = cls.__clean_key(key)

            cipher = AES.new(key, AES.MODE_EAX, nonce=chipher_collection["nonce"])
            plaintext = cipher.decrypt(chipher_collection["ciphertext"])

            cipher.verify(chipher_collection["tag"])
            return ploads(plaintext)
        except ValueError:
            raise SheepException("incorrect tag")
        except KeyError:
            raise SheepException("Missing chipher collection elements")
        except IndexError:
            raise SheepException("the key is to short")

    @classmethod
    def serial(cls, obj) -> bytes:
        return pdumps(obj)

    @classmethod
    def deserial(cls, seri: bytes) -> object:
        return ploads(seri)

    # still buggy
    @classmethod
    def encry_combination(cls, obj, pub_key) -> dict:
        random_key = token_hex(32)
        return {
            "sym_cipher": cls.encry_sym(obj, random_key),
            "encry_key": cls.encry_asym(random_key, pub_key)
        }

    @classmethod
    def decry_combination(cls, cipher, pri_key):
        if not (dec_key := cls.decry_asym(cipher["encry_key"], pri_key))[0]:
            raise SheepException
        return cls.decry_sym(cipher["sym_cipher"], dec_key[1])
