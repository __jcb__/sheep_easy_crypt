# SheepEasyCrypt
This Package comes from a Project about crytography. I was using this all the time. So I extracted it and now it will be reuploaded. \
It contains a symmetric and an asymmetric cipher.

## Symmetric
``` python
from sheep_crypt import SheepEasyCrypt as sec

TEXT = "Hello"
KEY = "SeCrEt!?"

# encryption
cipher = sec.encry_sym(TEXT, KEY)
print(cipher)

# decryption
text = sec.decry_sym(cipher, KEY)
print(text)
```

## Asymmetric
```python
from sheep_crypt import SheepEasyCrypt as sec

TEXT = "Hello"
PUBLIC, PRIVATE = sec.get_asym_key()

# encryption
cipher = sec.encry_asym(TEXT, PUBLIC)
print(cipher)

# decryption
text = sec.decry_asym(cipher, PRIVATE)
print(text)
```

## Combination
In addition to it is there a Cipher, which encrypts the data by a symmetric cipher and the key for it is being encrypted by a asymmetric cipher. 
``` python
from sheep_crypt import SheepEasyCrypt as sec

TEXT = "Hello"
PUBLIC, PRIVATE = sec.get_asym_key(1024)

# encryption
cipher = sec.encry_combination(TEXT, PUBLIC)
print(cipher)

# decryption
text = sec.decry_combination(cipher, PRIVATE)
print(text)

```



